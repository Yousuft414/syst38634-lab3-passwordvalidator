/**
 * @author Taha Yousuf
 * @ID: 991456090
 * 
 * This class is used as a password validator for a larger student registration system. The module will be implemented
 * with the use of TDD. 
 */
package application;

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	
	public static boolean hasValidCaseChars(String password) {
		return password != null && password.matches("^.*[a-z].*$") && password.matches("^.*[A-Z].*$");
	}
	
	/**
	 * 
	 * @param password
	 * @return true if number of characters is 8 or more. No spaces allowed
	 */
	public static boolean isValidLength(String password) {
		return password.indexOf(" ")<0 && password.trim().length() >= MIN_LENGTH;	
	}
	
	/**
	 * 
	 * @param password
	 * @return true if the password contains 2 or more digits. 
	 */
	public static boolean containsValidDigits(String password) {
		int numCount = 0;
		char entry;
		for (int i = 0; i<password.length(); i++) {
			entry = password.charAt(i);
			if (entry >= '0' && entry <= '9') {
				numCount++;
			}
		}
		return numCount >=2;
	}
}
