package application;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PasswordValidatorTest {
	
	@Test
	public void testHasVaildCaseCharsRegular() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("aBAaksiB"));
	}
	@Test
	public void testHasVaildCaseCharsBoundaryIn() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("hT"));
	}
	@Test
	public void testHasVaildCaseCharsException() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("787737"));
	}
	@Test
	public void testHasVaildCaseCharsExceptionSpecial() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("^%$#@!"));
	}
	@Test
	public void testHasVaildCaseCharsExceptionBlank() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(""));
	}
	@Test
	public void testHasVaildCaseCharsExceptionNull() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(null));
	}
	@Test
	public void testHasVaildCaseCharsBoundaryOutUpper() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("BYROTLS"));
	}
	@Test
	public void testHasVaildCaseCharsBoundaryOutLower() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("asdfrgr"));
	}
	@Test
	public void testIsValidLengthRegular() {
		boolean result = PasswordValidator.isValidLength("1234567890");
		assertTrue("Invalid Length", result);
	}
	@Test
	public void testIsValidLengthException() {
		boolean result = PasswordValidator.isValidLength("");
		assertFalse("Invalid Length", result);
	}
	@Test
	public void testIsValidLengthExceptionSpaces() {
		boolean result = PasswordValidator.isValidLength("   test     ");
		assertFalse("Invalid Length", result);
	}
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean result = PasswordValidator.isValidLength("12345678");
		assertTrue("Invalid Length", result);
	}
	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean result = PasswordValidator.isValidLength("1234567");
		assertFalse("Invalid Length", result);
	}
	
	@Test
	public void testContainsValidDigitsRegular() {
		boolean result = PasswordValidator.containsValidDigits("Password1234");
		assertTrue("Invalid number of digits", result);
	}
	@Test
	public void testContainsValidDigitsException() {
		boolean result = PasswordValidator.containsValidDigits("Password");
		assertFalse("Invalid number of digits", result);
	}
	@Test
	public void testContainsValidDigitsBoundaryIn() {
		boolean result = PasswordValidator.containsValidDigits("Password12");
		assertTrue("Invalid number of digits", result);
	}
	@Test
	public void testContainsValidDigitsBoundaryOut() {
		boolean result = PasswordValidator.containsValidDigits("Password1");
		assertFalse("Invalid number of digits", result);
	}

}
